import { StyleSheet } from 'react-native'

export default Styles = StyleSheet.create({
	wrapper: {
    flex: 1,
    padding: 30,
  },
  titleContainer:{
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 35,
  },
  title: {
    fontSize: 30,
  },
  TextInput: {
    fontSize: 20,
    marginBottom: 10,
    paddingHorizontal: 8,
    paddingVertical: 6,
    borderBottomWidth: 1,
    borderBottomColor: '#ddd',
  }, 
  contentContainerStyle:{
    paddingVertical: 20,
  }
});