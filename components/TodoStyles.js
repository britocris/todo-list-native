import { StyleSheet } from 'react-native'

export default Styles = StyleSheet.create({
	todos: {
    marginTop: 0,
	},
	todo: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    padding: 16,
    marginTop: 16,
    borderColor: '#bbb',
    borderWidth: 1,
    borderStyle: "dashed",
    borderRadius: 10,
  },
  todoText: {
    fontSize: 20,
  },
})