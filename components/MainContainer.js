import React, {useState} from 'react';
import Todo from './Todo'
import { Text, View, Button, TextInput, Alert, SafeAreaView, ScrollView } from 'react-native';
import Styles from './MainContainerStyles'

const initialState = {
  inputValue: '',
  addEditButton: {
    text: 'ADD',
    color: 'blue',
  },
  edit: {
    isEdit: false,
    ID: 0,
  },
  todos: [
    { text: 'Lavar' },
    { text: 'Barrer' },
    { text: 'Limpiar' },
    { text: 'Cocinar' },
  ],
}

const MainContainer = () => {
  const [state, setState] = useState(initialState);
  const { inputValue, todos, addEditButton, edit } = state;

  function AddToDo() {
    if(inputValue.length > 0) {
      if(!edit.isEdit){
        setState({
          ...state,
          inputValue: "",
          edit: { isEdit: false, ID: 0 },
          todos: [ ...todos, { text: inputValue }],
          addEditButton: { text: 'ADD', color: 'blue' }
        })
      }
      else{
        setState({
          ...state,
          inputValue: "",
          edit: { isEdit: false, ID: 0 },
          addEditButton: { text: 'ADD', color: 'blue' },
          todos: todos.map((todo, i) => {
            if(i === edit.ID) {
              return {...todo, text: inputValue}
            } else return todo
          }),
        })
      }
    }
    else {
      Alert.alert('Error', 'Debe ingresar una tarea', [
        {text: 'Entendido'}
      ])
    }
  }

  function deleteTodo(index) {

    if(edit.ID === index) {
      setState({
        ...state,
        todos: todos.filter((todo, i) => i !== index ),
        inputValue: '',
        addEditButton: { text: 'ADD', color: 'blue' },
        edit: { isEdit: false, ID: 0 }
      })
    } else {
      setState({
        ...state,
        todos: todos.filter((todo, i) => i !== index ),
        edit: { isEdit: edit.isEdit, ID: index > edit.ID ? edit.ID : edit.ID - 1 }
      })
    }
  }

  return (
    <View style={ Styles.wrapper }>
      <View style={ Styles.titleContainer }>
        <Text style={ Styles.title }>To Do List</Text>
      </View>
      <View style={ Styles.addToDoContainer }>
        <TextInput 
          value = { inputValue }
          style={ Styles.TextInput } 
          placeholder = "Agregar Tarea"
          onChangeText = { text => setState({...state, inputValue: text}) }
        />
        <Button 
          title={ addEditButton.text }
          color={ addEditButton.color }
          onPress={ AddToDo }
          />
      </View>
      <SafeAreaView style={{flex: 1, paddingTop: 30}}>
        <ScrollView>
          <Todo state={state} todos={todos} deleteTodo={deleteTodo} setState={setState}/>
        </ScrollView>
      </SafeAreaView>
    </View>
  );
}

export default MainContainer;