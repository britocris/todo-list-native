import React from 'react';
import { Text, View, Button, TouchableOpacity} from 'react-native';
import Styles from './TodoStyles'

const Todo = ({ todos, deleteTodo, state, setState }) => {

	function onPressTodo(text, index) {
		setState({
			...state, 
			inputValue: text,
			edit: {
				isEdit: true,
				ID: index,
			},
			addEditButton: { text: 'EDIT', color: 'green'
		}})
	}

	return (
		<View style={Styles.todos}>
      {todos && todos.map((todo, index) => ( 
        <View key={index} style={Styles.todo}>
          <TouchableOpacity 
            activeOpacity={0.6}
            onPress={() => onPressTodo(todo.text, index) }
            style={{flex: 1}}
          >
            <Text style={Styles.todoText} > 
              { todo.text } 
            </Text>
          </TouchableOpacity>
          <Button 
            color="red" 
            title="Delete"
            onPress={ () => deleteTodo(index) } 
          />
        </View>
      ))}
			</View>
	)
}

export default Todo;